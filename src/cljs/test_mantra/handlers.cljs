(ns test-mantra.handlers
  (:require [re-frame.core :as re-frame]
                        [mantra.core :as m]
              [test-mantra.db :as db]))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/register-handler
 :set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))


;; c d e f g a b
(def g5 783.99)
(def a5 880)
(def c6 1046.5)
(def b5 987.77)
(def e6 1318.51)
(def d6 1174.66)
(def f6 1396.91)
(def g6 1567.98)
;; g5 g5 a5 g5 c6 b5
;; g5 g5 a5 g5 c6 b5
;; g5 g5 f6 d6 c6 b5 a5
;; f6 f6 e6 c6 d6 c6
(def sq (m/osc :type :square))
(m/set-tempo 150)
(def line1 [{:pitch g5 :duration :eighth}
            {:pitch g5 :duration :eighth}
            {:pitch a5 :duration :quarter}
            {:pitch g5 :duration :quarter}
            {:pitch c6 :duration :quarter}
            {:pitch b5 :duration :half}
            {:duration :half}
            ])
(def line2 [{:pitch g5 :duration :eighth}
            {:pitch g5 :duration :eighth}
            {:pitch g6 :duration :quarter}
            {:pitch f6 :duration :quarter}
            {:pitch d6 :duration :quarter}
            {:pitch c6 :duration :quarter}
            {:pitch b5 :duration :quarter}
            {:pitch a5 :duration :half}
            {:duration :half}

            {:pitch f6 :duration :eighth}
            {:pitch f6 :duration :eighth}
            {:pitch e6 :duration :quarter}
            {:pitch c6 :duration :quarter}
            {:pitch d6 :duration :quarter}
            {:pitch c6 :duration :half}
            
            ])


(defn my-play []
  (m/stop-all-oscs)
  (m/play-notes sq
                (flatten (cons (repeat 2 line1)
                               line2))))


(re-frame/register-handler
 :play-song
 (fn [db _]
   (my-play)
   db))
