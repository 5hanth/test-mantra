(ns test-mantra.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [test-mantra.handlers]
              [test-mantra.subs]
              [test-mantra.routes :as routes]
              [test-mantra.views :as views]
              [test-mantra.config :as config]))

(when config/debug?
  (println "dev mode"))

(defn mount-root []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init [] 
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (mount-root))
