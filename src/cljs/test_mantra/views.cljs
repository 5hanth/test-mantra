(ns test-mantra.views
  (:require [re-frame.core :as re-frame]
              [re-com.core :as re-com]))

;; home

(defn home-title []
  (let [name (re-frame/subscribe [:name])]
    (fn []
      [re-com/title
                                        ;       :label (str "Hello from " @name ". This is the Home Page.")
       :label "Happy Birthday!!!"
       :level :level1])))

(defn mantra-button []
  [re-com/button
   :label "> Play"
   :on-click #(re-frame/dispatch [:play-song])])


(defn home-panel []
  [re-com/v-box
   :align :center
   :gap "1em"
   :children [[home-title]]])



;; main

(defmulti panels identity)
(defmethod panels :home-panel [] [home-panel])
(defmethod panels :default [] [:div])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [:active-panel])]
    (fn []
      [re-com/v-box
       :height "100%"
       :children [(panels @active-panel)]])))
